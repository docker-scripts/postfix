cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    ds copy-ssl-cert
    _create_cron_job_to_copy_ssl_cert

    ds inject setup.sh
    ds inject fail2ban.sh

    for domain in $(echo $VIRTUAL_DOMAINS); do
        ds dkimkey add $domain
    done

    [[ -f config/custom.sh ]] \
        || cp $APPDIR/misc/custom.sh config/

    ds inject update.sh
}

# create a cron job that copies the ssl cert from revproxy once a week
_create_cron_job_to_copy_ssl_cert() {
    local dir=$(basename $(pwd))
    mkdir -p /etc/cron.d
    cat <<EOF > /etc/cron.d/"$(echo $dir | tr . -)"-copy-ssl-cert
# copy the ssl cert @$dir each week
0 0 * * 0  root  bash -l -c "ds @$dir copy-ssl-cert &> /dev/null"
EOF
}
