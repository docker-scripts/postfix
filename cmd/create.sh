cmd_create_help() {
    cat <<_EOF
    create
        Create the container '$CONTAINER'.

_EOF
}

rename_function cmd_create orig_cmd_create
cmd_create() {
    chmod o=x .

    orig_cmd_create \
        --hostname $HOSTNAME \
        `# capability NET_ADMIN needed for iptables and fail2ban` \
        --cap-add=NET_ADMIN \
        "$@"    # accept additional options

    # get domain aliases
    local aliases=$(echo $VIRTUAL_DOMAINS | tr ' ' "\n" | sed -e 's/^/smtp./g' | sed -e "/^$HOSTNAME\$/d")

    # add domains to revproxy
    ds @revproxy domains-rm $HOSTNAME
    ds @revproxy domains-add $HOSTNAME $aliases

    # get a ssl cert
    ds @revproxy del-ssl-cert $HOSTNAME
    ds @revproxy get-ssl-cert $HOSTNAME $aliases
}
