cmd_test2_help() {
    local mydomain=$(echo $VIRTUAL_DOMAINS | cut -d' ' -f1)
    cat <<_EOF
    test2
        Make some tests about LDAP authentication, as described on:
        https://gitlab.com/docker-scripts/postfix/-/blob/master/docs/2-forward-only-accounts-with-ldap-auth.md
        It assumes that LDAP users (user1@$mydomain,pass1) and
        (user2@$mydomain,pass2) have been created, for example like this:
            ds ou add users
            ds user add users user1@$mydomain $FORWARD_ADDRESS pass1
            ds user add users user2@$mydomain $FORWARD_ADDRESS pass2
        Here 'users' is the OU on the LDAP_SEARCH_BASE

_EOF
}

runcmd() {
    echo -en "\n--> ds exec $@" && read
    dexec "$@"
}

cmd_test2() {
    local mydomain=$(echo $VIRTUAL_DOMAINS | cut -d' ' -f1)
    cat <<_EOF && read

These tests are about LDAP authentication and forward-only accounts,
as described on:
https://gitlab.com/docker-scripts/postfix/-/blob/master/docs/2-forward-only-accounts-with-ldap-auth.md

They assume that LDAP users (user1@$mydomain,pass1) and
(user2@$mydomain,pass2) have been created, for example like this:
    ds ou add users
    ds user add users user1@$mydomain $FORWARD_ADDRESS pass1
    ds user add users user2@$mydomain $FORWARD_ADDRESS pass2

Here 'users' is the OU on the LDAP_SEARCH_BASE
If these test data do not exist on LDAP, cancel this script (Ctrl+C),
add them, and try again.

_EOF
    echo '1. Testing LDAP queries'
    runcmd apt install --yes ldap-utils
    runcmd ldapsearch -x -LLL -ZZ \
           -H ldap://$LDAP_SERVER_HOST \
           -D $LDAP_BIND_DN -w $LDAP_BIND_PW \
           -b $LDAP_SEARCH_BASE \
           "(uid=user1@$mydomain)"
    runcmd apt purge --yes ldap-utils
    runcmd postmap -q user1@$mydomain ldap:/host/config/smtpd_sender_login_maps.ldap
    runcmd postmap -q user1@$mydomain ldap:/host/config/virtual_alias_maps.ldap
    read
    cat <<EOF && read
If the postmap queries did not return the expected result, append the
line 'debuglevel = -1' on 'smtpd_sender_login_maps.ldap' and 'virtual_alias_maps.ldap'
and try to identify the problem.
EOF

    echo '2. Test SASL authentication'
    runcmd cat /etc/saslauthd.conf
    runcmd cat /etc/postfix/sasl/smtpd.conf
    runcmd testsaslauthd \
           -f /var/spool/postfix/var/run/saslauthd/mux \
           -u user1 -r $mydomain -p pass1
    runcmd testsaslauthd \
           -f /var/spool/postfix/var/run/saslauthd/mux \
           -u user2 -r $mydomain -p pass2
    runcmd postconf smtpd_sasl_auth_enable
    read

    # install swaks and remove docker nets from trusted_hosts
    cat <<EOF && read
For the following tests we need to make sure that 'swaks' is installed
and that we are sending the emails from a host that is not in the list
of trusted_hosts.
EOF
    apt install --yes swaks
    sed -i config/trusted_hosts -e '/172.16.0.0/d'
    ds inject update.sh
    echo

    runtest "3. Try to send email without authentication

    swaks -tlso \\
        --server $HOSTNAME \\
        --from user1@$mydomain \\
        --to $FORWARD_ADDRESS

    This should fail. If it succeeds, make sure to remove the public
    IP of the server from 'config/trusted_hosts' and run 'ds inject
    update.sh'
    "\
            swaks -tlso \
            --server $HOSTNAME \
            --from user1@$mydomain \
            --to $FORWARD_ADDRESS

    runtest "4. Send email with authentication

    swaks -tlso \\
        --server $HOSTNAME \\
        --from user1@$mydomain \\
        --to $FORWARD_ADDRESS \\
        --auth-user user1@$mydomain \\
        --auth-password pass1
    "\
            swaks -tlso \
            --server $HOSTNAME \
            --from user1@$mydomain \
            --to $FORWARD_ADDRESS \
            --auth-user user1@$mydomain \
            --auth-password pass1

    runtest "5. Try to send email from the wrong address

    swaks -tlso \\
        --server $HOSTNAME \\
        --auth-user user1@$mydomain \\
        --auth-password pass1 \\
        --from user2@$mydomain \\
        --to $FORWARD_ADDRESS

    Using credentials of user1, we try to send email as user2.
    This should not be allowed.
    "\
            swaks -tlso \
            --server $HOSTNAME \
            --auth-user user1@$mydomain \
            --auth-password pass1 \
            --from user2@$mydomain \
            --to $FORWARD_ADDRESS

    runtest "6. Test forwarding emails

    swaks -tlso \\
        --server $HOSTNAME \\
        --auth-user user1@$mydomain \\
        --auth-password pass1 \\
        --from user1@$mydomain \\
        --to user2@$mydomain

    Let's send an email from user1 to user2. It should be forwarded to
    the external email address of user2.
    "\
            swaks -tlso \
            --server $HOSTNAME \
            --auth-user user1@$mydomain \
            --auth-password pass1 \
            --from user1@$mydomain \
            --to user2@$mydomain

    # append docker nets to trusted_hosts
    echo '172.16.0.0/12' >> config/trusted_hosts
    ds inject update.sh
}
