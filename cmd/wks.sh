cmd_wks_help() {
    cat <<_EOF
    wks del <wks-server>
    wks add <wks-server> <port> <mail-domain> [...]
        Setup one or more mail domains to be served by the given WKS server.

_EOF
}

cmd_wks() {
    local cmd=$1
    shift
    case $cmd in
        del) _wks_del "$@" ;;
        add) _wks_add "$@" ;;
        *)   echo -e "Usage:\n$(cmd_wks_help)\n" ;;
    esac
}

_wks_del() {
    local wks_server=$1
    [[ -z $wks_server ]] && fail "Usage:\n$(cmd_wks_help)\n"

    # remove any virtual aliases
    sed -i config/virtual_alias_maps -e "/keys@$wks_server/d"
    ds exec postmap config/virtual_alias_maps

    # remove any routes
    sed -i config/transport_maps -e "/^$wks_server /d"
    ds exec postmap config/transport_maps

    # rewrite webkey address on outgoing mails
    sed -i config/smtp_generic_maps -e "/^webkey@$wks_server /d"
    ds exec postmap config/smtp_generic_maps

    # remove ip from trusted_hosts
    local wks_ip=$(getent ahostsv4 $wks_server | grep STREAM | head -1 | cut -d' ' -f1)
    sed -i config/trusted_hosts -e "/^$wks_ip\$/d"

    # update postfix config
    ds inject update.sh &>/dev/null
}

_wks_add() {
    local wks_server=$1 port=$2
    shift 2
    local domains="$@"
    [[ -z $wks_server || -z $port || -z $domains ]] \
        && fail "Usage:\n$(cmd_wks_help)\n"

    # add virtual aliases
    for mail_domain in $domains; do
        sed -i config/virtual_alias_maps \
            -e "/^keys@$mail_domain/d"
        echo "keys@$mail_domain  keys@$wks_server" >> config/virtual_alias_maps
    done
    ds exec postmap config/virtual_alias_maps

    # how to send the mails to the WKS server
    ds exec postconf 'transport_maps = hash:/host/config/transport_maps'
    touch config/transport_maps
    sed -i config/transport_maps \
        -e "/^$wks_server /d"
    echo "$wks_server smtp:[$wks_server]:$port" >> config/transport_maps
    ds exec postmap config/transport_maps

    # rewrite webkey address on outgoing mails
    ds exec postconf 'smtp_generic_maps = hash:/host/config/smtp_generic_maps'
    touch config/smtp_generic_maps
    local mail_domain=$(echo $domains | cut -d' ' -f1)
    grep -q "webkey@$wks_server" config/smtp_generic_maps \
         || echo "webkey@$wks_server  keys@$mail_domain" >> config/smtp_generic_maps
    ds exec postmap config/smtp_generic_maps

    # allow WKS server to send mails without authentication
    local wks_ip=$(getent ahostsv4 $wks_server | grep STREAM | head -1 | cut -d' ' -f1)
    grep -q "^$wks_ip\$" config/trusted_hosts \
        || echo $wks_ip >> config/trusted_hosts

    # update postfix config
    ds inject update.sh &>/dev/null
}
