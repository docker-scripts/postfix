cmd_test1_help() {
    cat <<_EOF
    test1
        Send some basic test emails, which are described on:
        https://gitlab.com/docker-scripts/postfix/-/blob/master/docs/1-send-email-only-from-trusted-hosts.md

_EOF
}

cmd_test1() {
    hash swaks 2>/dev/null \
	|| fail "Error: Dependency 'swaks' is missing. \nInstall it first (apt install swaks) and try again."

    local mydomain=$(echo $VIRTUAL_DOMAINS | cut -d' ' -f1)

    runtest "1. Send email from inside the container

    docker exec swaks \\
        --from noreply@$mydomain \\
        --to info@$mydomain

    This test should succeed because the recipient (info@$mydomain)
    exists on the 'virtual_alias_maps'. The sender can be anything (in
    this case 'noreply') and the email will still be sent, since we are
    sending from localhost, which is a trusted host.

    This email is actually forwarded to the address that is listed on
    'virtual_alias_maps' (in this case $FORWARD_ADDRESS).
    Check also the spam folder if you cannot find it on the inbox.

    "\
            dexec swaks \
            --from noreply@$mydomain \
            --to info@$mydomain

    runtest "2. Send email to non-existing address

    docker exec swaks \\
        --from info@$mydomain \\
        --to noreply@$mydomain

    This test should fail because the recipient (noreply@$mydomain)
    does not exist on 'virtual_alias_maps', so the mailserver does not
    know what to do with this email.

    "\
            dexec swaks \
            --from info@$mydomain \
            --to noreply@$mydomain

    runtest "3. Send email to an external account

    docker exec swaks -tlso \\
        --server localhost \\
        --from info@$mydomain \\
        --to $FORWARD_ADDRESS

    "\
            dexec swaks -tlso \
            --server localhost \
            --from info@$mydomain \
            --to $FORWARD_ADDRESS

    runtest "4. Send email from the host (outside the container)

    swaks \\
        --from info@$mydomain \\
        --to admin@$mydomain

    If this fails, add the IP of the host ('$(get_public_ip)')
    on the list of trusted hosts ('config/trusted_hosts') and then run
    'ds inject update.sh'

    "\
            swaks \
            --from info@$mydomain \
            --to admin@$mydomain

    runtest "5. Send email on the port 587

    swaks \\
        --server $HOSTNAME:587 \\
        --from info@$mydomain \\
        --to admin@$mydomain

    "\
            swaks \
            --server $HOSTNAME:587 \
            --from info@$mydomain \
            --to admin@$mydomain

    runtest "6. Send email on the port 465

    swaks \\
        --server $HOSTNAME:465 \\
        --from info@$mydomain \\
        --to admin@$mydomain

    "\
            swaks \
            --server $HOSTNAME:465 \
            --from info@$mydomain \
            --to admin@$mydomain

    runtest "7. Send email to automatic verifier

    swaks -tlso \\
        --server $HOSTNAME \\
        --from info@$mydomain \\
        --to check-auth@verifier.port25.com

    The automatic reply will give you important information about the
    status and health of the email server (for example whether the
    mails sent from it pass the SPF and DKIM checks, whether they are
    considered spam or not, etc.)

    "\
            swaks -tlso \
            --server $HOSTNAME \
            --from info@$mydomain \
            --to check-auth@verifier.port25.com
}
