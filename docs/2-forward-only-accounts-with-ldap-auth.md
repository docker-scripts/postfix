
# Forward-Only email accounts with LDAP authentication

**Note:** This setup extends the basic one with LDAP
authentication. So, before going on here, make sure that you have
already followed the instructions on: [Send email only from trusted
hosts](/docs/1-send-email-only-from-trusted-hosts.md)

## 1. Introduction

In this case, the mail server allows you to send email only if you
authenticate with a valid username and password (which is checked on
the LDAP server).

If someone sends email to one of the mail domains supported by the
server, the recipient address will be checked on the LDAP directory
whether it is valid, before the mail is accepted. The received emails
are not stored locally, but are forwarded instead to an external email
address that belongs to the user (and is recorded in the LDAP
directory).

For this setup we obviously need to have an LDAP server that contains
the user details (username, password and forward email). So, first of
all we will see how to install such a server.

## 2. Setup a LDAP server

### 2.1. Install OpenLDAP container

Let's assume that you have already installed
[docker-scripts](/docs/1-send-email-only-from-trusted-hosts.md#31-install-docker-scripts)
and
[revproxy](/docs/1-send-email-only-from-trusted-hosts.md#32-install-revproxy)
from the previous guide. Follow these steps to install OpenLDAP:

- Get the scripts: `ds pull openldap`

- Initialize a directory: `ds init openldap @ldap.example.org`

- Edit settings and make sure to uncomment the `DOMAIN` as well:

  ```
  cd /var/ds/ldap.example.org/
  vim settings.sh
  ```

- Make the container: `ds make`

- Check the container:

  ```
  cd /var/ds/ldap.example.org/
  ds shell
  hostname    # must be equal to $HOSTNAME
  ps ax
  systemctl status slapd
  slapcat
  slapcat -n0    # cn=config database
  ds exit
  ```

### 2.2. Add data to LDAP

- Add organizational units `users` and `apps`:

  ```
  ds ou
  ds ou add users
  ds ou add apps
  ds ou ls
  ```
  
  The unit `users` is for the email users, and `apps` is for the mail
  server and other applications that need to read the user data from
  LDAP.

- Add some test users:

  ```
  ds user
  ds user add users user1@example.org abc@mail.com pass123
  ds user add users user2@example.org xyz@mail.com
  ds set-passwd pass234 uid=user2@example.org,ou=users,dc=example,dc=org
  ds user ls
  ```

  **Note:** The users that are created with `ds user add` have a
  minimal data structure that looks like this:
  
  ```
  dn: uid=user1@example.org,ou=users,dc=example,dc=org
  uid: user1@example.org
  objectClass: top
  objectClass: inetOrgPerson
  sn: user1@example.org
  cn: user1@example.org
  mail: abc@mail.com
  userPassword:: e1NTSEF9OUU2VWhqUXNNY2wvcGdicTExR3FBNXRnOGduUmxhMmM=
  ```
  
  This is sufficient for the purpose of keeping forward email
  accounts. But if you have to customize it, make a local copy of
  `cmd/user.sh` and modify it as needed:
  
  ```
  cd /var/ds/ldap.example.org/
  mkdir -p cmd/
  cp /opt/docker-scripts/openldap/cmd/user.sh cmd/
  vim cmd/user.sh
  ```

- Create an `apps` user for the mail server, so that it can read the
  data from LDAP:

  ```
  ds app
  ds app add mail1 pass123
  ds app ls
  ```
  
- Check that the app user `cn=mail1,ou=apps,dc=example,dc=org` can
  read data from the server:
  
  ```
  apt install ldap-utils
  ldapsearch -xLLL \
      -H ldap://ldap.example.org/ \
      -D cn=mail1,ou=apps,dc=example,dc=org -w pass123 \
      -b dc=example,dc=org "(uid=user1@example.org)"
  ldapsearch -xLLL -ZZ \
      -H ldap://ldap.example.org/ \
      -D cn=mail1,ou=apps,dc=example,dc=org -w pass123 \
      -b dc=example,dc=org "(uid=user1@example.org)"
  ```
  
  The option `-ZZ` is for using a **TLS** connection.
  
  If there is any problem, add the option `-d9` for debugging.
  

## 3. Enable LDAP authentication on the SMTP server

- Go to SMTP app directory: `cd /var/ds/smtp.example.org/`
  
- Edit `settings.sh` and uncomment the LDAP settings, giving them
  proper values:
  
  ```
  LDAP_SERVER_HOST="ldap.example.org"
  LDAP_SEARCH_BASE="ou=users,dc=example,dc=org"
  LDAP_BIND_DN="cn=mail1,ou=apps,dc=example,dc=org"
  LDAP_BIND_PW="pass123"
  ```

- Re-make the container: `ds make`

## 4. Testing

**Note:** For a quick automated test of the basic functionality of the
server try: `ds test2`. It assumes that the test users
(user1@example.org, pass1) and (user2@example.org, pass2) exist on the
LDAP database. They can be added like this:

```
cd /var/ds/ldap.example.org/
ds ou add users
ds user
ds user add users user1@example.org forward-address@mail.com pass1
ds user add users user2@example.org forward-address@mail.com pass2
```

### 4.1. Test the LDAP queries

```
cat config/smtpd_sender_login_maps.ldap
cat config/virtual_alias_maps.ldap

ds shell

postconf -f virtual_alias_maps
postconf -f smtpd_sender_login_maps

postmap -q user1@example.org ldap:/host/config/virtual_alias_maps.ldap
postmap -q user1@example.org ldap:/host/config/smtpd_sender_login_maps.ldap
```

If the postmap queries don't return the expected result, append the
line `debuglevel = -1` on `smtpd_sender_login_maps.ldap` and
`virtual_alias_maps.ldap` and try to identify the problem. You can
also try with `ldapsearch`, like this:

```
apt install ldap-utils
ldapsearch -x -LLL -ZZ \
    -H ldap://ldap.example.org/ \
    -D cn=mail1,ou=apps,dc=example,dc=org -w pass123 \
    -b dc=example,dc=org \
    "(uid=user1@example.org)"
```

### 4.2. Test SASL authentication

```
ds shell
cat /etc/saslauthd.conf
cat /etc/postfix/sasl/smtpd.conf
cat /etc/default/saslauthd | grep MECHANISMS
systemctl status saslauthd
testsaslauthd \
    -f /var/spool/postfix/var/run/saslauthd/mux \
    -u user1 -r example.org -p pass123
postconf smtpd_sasl_auth_enable
```

### 4.3. Test sending and forwarding emails

These test commands are supposed to run on a computer other than the
server, which has a public IP that is NOT on the list of trusted hosts
(`config/trusted_hosts`). While running them, it may be useful to check
the logs on the mail server, on a separate tab/terminal:

```
cd /var/ds/smtp.example.org/
ds shell
tail /var/log/mail.log -f
```

### 4.3.1 Try to send message without authentication

Send a test message from `user1@example.org` to a gmail account:

```
swaks --server smtp.example.org \
      --from user1@example.org --to user@gmail.com
[. . .]
<** 450 4.3.2 Service currently unavailable
[. . .]

swaks --server smtp.example.org \
      --from user1@example.org --to user@gmail.com
[. . .]
<** 554 5.7.1 <user@gmail.com>: Relay access denied
[. . .]
```

The first time you try to send an email from a new client the service
is automatically refused (in order to avoid spambots).  The second
time it fails because the sender is not authenticated.

**Note:** If you succeed to send email without authentication, make
sure that your public IP is not on `config/trusted_hosts`. If you
modify this file you should also run `ds inject update.sh`.
  
#### 4.3.2. Send a message with authentication

```
swaks --server smtp.example.org \
      --from user1@example.org \
      --to user@gmail.com \
      --auth-user user1@example.org \
      --auth-password pass123
[. . .]
<-  235 2.7.0 Authentication successful
[. . .]
```

This time it should succeed, if the password is correct.

#### 4.3.3. Send a message from the wrong address

Let's try to send a message from the same user as before, but with
`--from user2@example.org`:

```
swaks --server smtp.example.org \
      --auth-user user1@example.org \
      --auth-password pass123 \
      --from user2@example.org \
      --to user@gmail.com \
[. . .]
<-  235 2.7.0 Authentication successful
[. . .]
<** 553 5.7.1 <user2@example.org>: Sender address rejected: not owned by user user1@example.org
[. . .]
```

This `--from` address does not belong to the authenticated user, so
the server refuses to send the mail.


#### 4.3.4. Test forwarding emails

Let's send an email from `user1` to `user2`. It should be forwarded to
the external email address of `user2`:

```
swaks --server smtp.example.org \
      --auth-user user1@example.org \
      --auth-password pass123 \
      --from user1@example.org \
      --to user2@example.org \
```

Try also to send an email from an external email address to
`user1@example.org` and `user2@example.org`.


## Related tutorials

- http://acidx.net/wordpress/2014/06/installing-a-mailserver-with-postfix-dovecot-sasl-ldap-roundcube/
- https://www.vennedey.net/resources/2-LDAP-managed-mail-server-with-Postfix-and-Dovecot-for-multiple-domains
