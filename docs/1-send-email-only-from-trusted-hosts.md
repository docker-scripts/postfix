
# Send email only from trusted hosts

## 1. Introduction

Quite often web applications (like Gitea, NextCloud, Moodle, etc.)
need to send email notifications. For example Moodle needs to notify
students and teachers about various events. Without being able to send
notifications, Moodle and many other web applications loose half of
their usefulness. On many web applications you cannot even finish the
registration process and cannot login, unless you verify your email
address (the application sends you an email with a link that you have
to click).

To send emails, an application needs to contact by SMTP a local or
remote mail server. Installing a mail server is not so easy because it
needs also some DNS and other configurations, in order to do it
properly, otherwise the mails that are sent will end up being
classified as spam and most probably will not reach the recipient.


## 2. Minimal DNS configuration

In order to build a mail server you need to own a domain (say
`example.org`) and be able to customize its DNS records.

For each email domain you need something like this on the DNS
configuration:

```
smtp.example.org.    IN    A           10.11.12.13
example.org.         IN    MX    10    smtp.example.org.
example.org.         IN    MX    20    smtp.example.org.
example.org.         IN    TXT         "v=spf1 mx -all"
```

**Note:** Yes, both MX records point to the same server, it is not a
typo. It is done because the configuration of the server enables
postscreen deep protocol tests in order to block spambots (for more
details see
[this](https://www.linuxbabe.com/mail-server/configure-postscreen-in-postfix-to-block-spambots)).
This means that the server disconnects new/unknown SMTP clients when
they connect for the first time, even if they are legitimate. A
legitimate client usually goes on to try the second mailserver in the
list, and the second time they try to connect they are accepted.

The last line basically tells to the other SMTP servers that only this
server is allowed to send emails on behalf of this domain, and no
other servers. This is done to prevent spammers from faking your email
addresses. If a spammer tries to send a mail as if it is coming from
your domain, the SMTP server that is getting this email will consult
this DNS record, will figure out that the server of the spammer is not
allowed to send emails on behalf of `example.org`, and will
immediately close the connection to the spammer.

You can use `dig` to verify that these DNS records have been activated:

```
$ dig MX example.org +short
10 smtp.example.org.
20 smtp.example.org.

$ dig A smtp.example.org +short
10.11.12.13

$ dig TXT example.org +short
"v=spf1 mx -all"
```

Depending on your DNS server, DNS changes may take from a few minutes
to a couple of days to propagate.

## 3. Build a postfix container with docker-scripts

It is easy to build a postfix container with docker-scripts.

### 3.1. Install docker-scripts

```
sudo su
apt install m4 make git
git clone https://gitlab.com/docker-scripts/ds /opt/docker-scripts/ds
cd /opt/docker-scripts/ds/
make install
```

### 3.2. Install revproxy

- Get the scripts: `ds pull revproxy`
- Initialize a directory: `ds init revproxy @revproxy`
- Fix the settings: `cd /var/ds/revproxy/; vim settings.sh`
- Make the container: `ds make`

We need `revproxy` to get and manage letsencrypt SSL certificates for
the `postfix` container.

### 3.3. Install postfix

- Get the scripts: `ds pull postfix`
- Initialize a directory: `ds init postfix @smtp.example.org`
- Fix the settings: `cd /var/ds/smtp.example.org/ ; vim settings.sh`
- Make the container: `ds make`

Check the installation:

```
cd /var/ds/smtp.example.org/

ls sslcert/
ls /etc/cron.d/

ls config/
cat config/trusted_hosts
cat config/virtual_alias_maps
cat config/virtual_alias_maps.regexp
ls config/dkim-keys/

ds shell
ps ax
systemctl status postfix
ls /etc/postfix/
postconf -nf | less
tail /var/log/mail.log
exit
```

## 4. Make the mail server trustworthy

### 4.1. Activate a DKIM key

[DKIM](https://en.wikipedia.org/wiki/DomainKeys_Identified_Mail) keys
are used by a mail server to sign the emails that it sends, so that
the emails cannot be changed in transit, and so that the receiver can
verify that this server is authorized to send emails for a domain. It
is an important tool against spams and faked emails. If an smtp server
signs the messages that it sends, it is less likely that they will be
classified as spam.

Installation scripts generate a DKIM key as well, which is on
`config/dkim-keys/example.org/`.  To activate it you need to add a
record like this on the DNS configuration of the domain:

```
mail._domainkey.example.org.  IN  TXT  "v=DKIM1; h=sha256; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQE....kMJdAwIDAQAB"
```

You can find the content of the public key on the file:
`config/dkim-keys/example.org/mail.txt`.

To check whether it has been activated or not, try the command:

```
dig TXT mail._domainkey.example.org +short
```

### 4.2. Create a DMARC record

[DMARC](https://postmarkapp.com/support/article/892-what-is-dmarc) is
a standard that allows you to set policies on who can send email for
your domain based on DKIM and SPF.

You can add a DMARC Record on DNS that will allow you to get weekly
reports from major ISPs about the usage of your email domain.

- Go to http://dmarc.postmarkapp.com/ and add your email address where
  you want to receive reports, and email domain name (`example.org`).

- On the DNS configuration of the domain add a TXT record like this:

  ```
  _dmarc.example.org.  IN  TXT  "v=DMARC1; p=none; pct=100; rua=mailto:re+x2i0yw1hoq7@dmarc.postmarkapp.com; sp=none; aspf=r;"
  ```

  The value of this TXT record is the one generated by the website
  above.

- To check that it has been activated, try the command:

  ```
  dig TXT _dmarc.example.org. +short
  ```

## 5. Test the SMTP server

To send test emails we use `swaks`: `apt install swaks`

**Note:** Another way to send test emails (instead of `swaks`) is by
using `curl` and a script `testmail.sh` with a content like this:

```
#!/bin/bash

from_address='info@example.org'
to_address='admin@example.org'
cat << EOF | curl -v --ssl --upload-file - \
                  --url 'smtp://smtp.example.org' \
                  --mail-from $from_address \
                  --mail-rcpt $to_address
From: $from_address
To: $to_address
Subject: test $(date)

Test message.
EOF
```

**Note:** For a quick automated test of the basic functionality of the
server try: `ds test1`

### 5.1. Send test emails from inside the container

  ```
  cd /var/ds/smtp.example.org/
  ds shell
  swaks --from noreply@example.org --to info@example.org
  tail /var/log/mail.log
  swaks --from info@example.org --to noreply@example.org
  tail /var/log/mail.log
  cat /host/config/virtual_alias_maps
  cat /host/config/virtual_alias_maps.regexp
  ```
  
  The first test succeeds because the recipient (`info@example.org`)
  exists on the `virtual_alias_maps`. The sender can be anything (in
  this case `noreply`) and the email will still be sent, since we are
  sending from localhost, which is a trusted host.
  
  This email is actually forwarded to the address that is listed on
  `virtual_alias_maps`. Check also the spam folder if you cannot find
  it on the inbox.
  
  The second test fails because the recipient (`noreply@example.org`)
  does not exist on `virtual_alias_maps`, so the mailserver does not
  know what to do with this email.
  
  **Note:** If you edit `virtual_alias_maps` and add a line like this:
  
  ```
  @example.org user@mail.com
  ```
  
  then the second test will succeed as well and the email will be
  forwarded to the given address. But first you will have to update
  the map with:
  
  ```
  postmap config/virtual_alias_maps
  ```
  
### 5.2. Send test emails to a gmail account

  ```
  swaks --from info@example.org --to username@gmail.com -tlso
  tail /var/log/mail.log
  swaks --from noreply@example.org --to username@gmail.com -tlso
  tail /var/log/mail.log
  ```
  
  Both of these tests will succeed. The option `-tlso` enables the TLS
  encryption of the connection.
  
  On gmail use "Show original" from the menu, to see the source of the
  received email.
  
  You can also send email from an external account to `info`, but you
  cannot send to `noreply` (unless the catch-all is enabled).

### 5.3. Send a test email from the host (outside the container)

  ```
  cd /var/ds/smtp.example.org/
  apt install swaks
  swaks --from info@example.org --to admin@example.org -tlso
  ```
  
  It will fail, because the IP of the host may not be on the list of
  the trusted hosts. Add it on `config/trusted_hosts` and then run `ds
  inject update.sh`. Verify that now it works:
  
  ```
  swaks --from info@example.org --to admin@example.org -tlso
  ```

### 5.4. Add a new recipient address

  Try to send email to `test@example.org`:

  ```
  swaks --from info@example.org --to test@example.org -tlso
  ...
  <** 550 5.1.1 <test@example.org>: Recipient address rejected: User unknown in virtual alias table
  ...
  ```
  
  It will fail because the recipient does not exist on the alias table.
  On `config/virtual_alias_maps` add a line like this:
  
  ```
  test@example.org  username@gmail.com
  ```
  
  Then update the alias db: `ds exec postmap
  /host/config/virtual_alias_maps` (or `ds inject update.sh`).
  Verify that now you can send email to this address:

  ```
  swaks --from info@example.org --to test@example.org -tlso
  ```

### 5.5. Send email to the ports `587` and `465`

  ```
  swaks --server smtp.example.org:587 \
        --from info@example.org --to admin@example.org
  swaks --server smtp.example.org --port 465 \
        --from info@example.org --to admin@example.org
  ```

## 6. Check the health of the mail server

- Send an email to `check-auth@verifier.port25.com`:

  ```
  swaks --server smtp.example.org -tlso \
        --from info@example.org --to check-auth@verifier.port25.com
  ```
  
  The automatic reply will give you important information about the
  status and health of your email server (for example whether the mails
  sent from it pass the SPF and DKIM checks, whether they are
  considered spam or not, etc.)

- Go to https://www.mail-tester.com/ and send a message to the email
  address displayed there, like this:
  
  ```
  swaks --server smtp.example.org -tlso \
        --from info@example.org --to test-1p4f6@mail-tester.com
  ```
  
  Then click the button for checking the score.

- Try https://github.com/drwetter/testssl.sh/

  ```
  git clone --depth 1 https://github.com/drwetter/testssl.sh.git
  cd testssl.sh/
  ./testssl.sh -t smtp smtp.example.org:25
  ```
  
- There are lots of other tools and websites that help to check the
  configuration of a mail server (DNS settings, configuration,
  security features, etc.) These are some of them:

  - https://ns.tools

  - https://mxtoolbox.com/

  - https://app.dmarcanalyzer.com
    + https://app.dmarcanalyzer.com/dns/spf?simple=1
    + https://app.dmarcanalyzer.com/dns/dkim?simple=1
    + https://app.dmarcanalyzer.com/dns/dmarc_validator


## 7. Add another email domain

The same smtp server can support more than one mail domains. If we
want to add another mail domain, for example `example.com`, we have to
do these:

1. Set DNS configurations like this:

   ```
   ; mail for example.com
   smtp.example.com.   IN   CNAME    smtp.example.org.
   example.com.    IN    MX    10    smtp.example.com.
   example.com.    IN    MX    20    smtp.example.com.
   example.com.    IN    TXT         "v=spf1 mx -all"
   ```

   Note that the first record (`CNAME`) redirects the new smtp domain
   to the first one.

   You can check these DNS configurations like this:

   ```
   dig +short smtp.example.com. A
   dig +short example.com. MX
   dig +short example.com. TXT
   ```

1. Edit `settings.sh` and add the new domain on `VIRTUAL_DOMAINS`:

   ```
   VIRTUAL_DOMAINS='
       example.org
       example.com
   '
   ```

1. Run `ds make` to rebuild the container. The rebuild is needed
   because we need to request a new SSL cert from letsencrypt, which
   covers all the domains (including the new one).

1. Go to http://dmarc.postmarkapp.com/ and generate a DMARC record for
   the new domain.

1. Update the DNS configuration with records like these:

   ```
   mail._domainkey.example.com.  IN  TXT  "v=DKIM1; h=sha256; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQE....kMJdAwIDAQAB"

   _dmarc.example.com.           IN  TXT  "v=DMARC1; p=none; pct=100; rua=mailto:re+x2i0yw1hoq7@dmarc.postmarkapp.com; sp=none; aspf=r;"
   ```

   Note that:
   + The value of the key for the DKIM record can be found on the file:
     `config/dkim-keys/example.com/mail.txt`
   + The value of the DMARC record is the one obtained on the previous
     step.

   You can check them like this:

   ```
   dig +short mail._domainkey.example.com. TXT
   dig +short _dmarc.example.com. TXT
   ```


## 8. Using the SMTP server

Different applications have different methods for configuring the SMTP
server. Let's see how to send emails from cron jobs and from Moodle.

**Important:** For this to work, the IP of the application server
should be on the list `config/trusted_hosts` on the smtp server,
otherwise it will refuse to accept and send emails. After adding it on
this list, run `ds inject update.sh` to update the configuration of
the mail server.

### 8.1. Sending emails from cron jobs

Cron jobs (for example `logwatch`) send emails to `root` through
`sendmail`. We can make it work with `ssmtp`. First install it with:
`apt install ssmtp`.  Then edit `/etc/ssmtp/ssmtp.conf` like this:

```
mailhub=smtp.example.org
rewriteDomain=example.org
UseSTARTTLS=YES
FromLineOverride=YES
```

Test it with: `echo test | sendmail -v root`


### 8.2. Sending emails from Moodle

If we search for `smtp` on the GUI menu for administration, we will
find that the place for SMTP configuration is on `Dashboard > Site
administration > Server > Email > Outgoing mail configuration` (or on
the location: `/admin/settings.php?section=outgoingmailconfig`).

But we can also configure Moodle from command line, like this:

```
moosh config-set smtphosts smtp.example.org
moosh config-set smtpsecure TLS
moosh config-set smtpauthtype PLAIN
moosh config-set smtpuser ''
moosh config-set smtppass ''
moosh config-set smtpmaxbulk 100
```


## 9. Related pages

- https://www.linux.com/learn/how-set-virtual-domains-and-virtual-users-postfix
- https://tecadmin.net/send-email-smtp-server-linux-command-line-ssmtp/
- https://blog.kruyt.org/postfix-and-tls-encryption/
- https://www.linuxbabe.com/mail-server/setting-up-dkim-and-spf
- https://tecadmin.net/setup-dkim-with-postfix-on-ubuntu-debian/
- https://www.skelleton.net/2015/03/21/how-to-eliminate-spam-and-protect-your-name-with-dmarc/
