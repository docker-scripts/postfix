# Send Email From GMail With SSMTP

Quite often web applications need to send email notifications. To send
emails, an application needs to contact by SMTP a local or remote mail
server. A relatively easy solution is to send emails from GMail SMTP
servers, on behalf of the web application. This has some limitations,
but for some applications it may be OK. It can be done with *Simple
SMTP* as follows:

1. First install *Simple SMTP*: `apt install ssmtp`

2. You can create a new GMail account or use an existing one. In both
   cases you need to enable the two-factor authentication on the
   google account, and then to create a new application-specific
   password on this account, as described here:
   https://www.lifewire.com/get-a-password-to-access-gmail-by-pop-imap-2-1171882

3. Edit `/etc/ssmtp/` and place a content like this:

   ```
   root=username@gmail.com
   mailhub=smtp.gmail.com:587
   AuthUser=username@gmail.com
   AuthPass=xyzxyzxyzxyzxyzxyz
   UseTLS=YES
   UseSTARTTLS=YES
   rewriteDomain=gmail.com
   hostname=localhost
   FromLineOverride=YES
   ```
   
   Here `AuthPass` is the app-specific password generated on the step above.

4. Add this line on `/etc/ssmtp/revaliases`:

   ```
   root:username@gmail.com:smtp.gmail.com:587
   ```

To test that it works, create and execute a script called `test-ssmtp.sh` with a content like this:

```
#!/bin/bash -x

recipient=${1:-user@example.org}

cat <<EOF | sendmail -v $recipient
To: $recipient
Subject: Testing ssmtp

Line 1
Line 2
EOF
```

**Note:** Replace `user@example.org` with your email address or call
the script with your email address as an argument.
