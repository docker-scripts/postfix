#!/bin/bash -x

cat /host/config/trusted_hosts > /etc/opendkim/trusted.hosts

trusted_hosts=$(cat /host/config/trusted_hosts | xargs | tr ' ' ',')
sed -i /etc/postfix-policyd-spf-python/policyd-spf.conf \
    -e "/skip_addresses/ c skip_addresses = $trusted_hosts"

postmap /host/config/virtual_alias_maps
postmap /host/config/virtual_alias_maps.regexp
postmap /host/config/smtp_header_checks

[[ -x /host/config/custom.sh ]] && /host/config/custom.sh

systemctl reload opendkim postfix
