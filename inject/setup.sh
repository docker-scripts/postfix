#!/bin/bash -x

source /host/settings.sh

main() {
    basic_setup
    tls_settings
    setup_postsrsd
    setup_spf_policy
    setup_opendkim

    if [[ -n $LDAP_SERVER_HOST ]]; then
        setup_ldap
        setup_saslauthd
    fi

    prevent_spam_early
    setup_postscreen
    setup_postwhite
    setup_pflogsumm

    systemctl restart postfix
}

postconf-get() {
    echo $(postconf $1 | cut -d= -f2 | tr -d ,)
}
postconf-set() {
    local option=$1 ; shift
    local values="$*"

    postconf -e "$option = "
    echo $values \
        | tr ' ' '\n' \
        | sed 's/^/    /g' \
        | sed -i /etc/postfix/main.cf \
              -e "/^$option =/r /dev/stdin"
}
postconf-append() {
    local option=$1 ; shift
    local new_values="$*"
    postconf-set $option $(postconf-get $option) $new_values
}

get_public_ip() {
    local services='ifconfig.me icanhazip.com'
    local pattern='^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$'
    local ip
    for service in $services; do
        ip=$(curl -s $service)
        [[ $ip =~ $pattern ]] && echo $ip && return
    done
    return 1
}

basic_setup() {
    # for more details see:
    # https://www.linuxbabe.com/mail-server/postfix-send-only-smtp-server-centos-8

    local mydomain=$(echo $VIRTUAL_DOMAINS | cut -d' ' -f1)
    local public_ip=$(get_public_ip)
    local myhostname=$(dig -x $public_ip +short)
    myhostname=${myhostname%.}
    myhostname=${myhostname:-$HOSTNAME}
    postconf \
        "mydomain = $mydomain" \
        "myorigin = $mydomain" \
        "mydestination = localhost" \
        "myhostname = $myhostname" \
        "proxy_interfaces = $public_ip"

    # configuration files that can be customized further by the user
    postconf \
        'mynetworks = /host/config/trusted_hosts' \
        'virtual_alias_domains = /host/config/virtual_alias_domains'
    postconf-set \
        'virtual_alias_maps' \
        hash:/host/config/virtual_alias_maps \
        regexp:/host/config/virtual_alias_maps.regexp

    # mynetworks
    mkdir -p /host/config
    [[ -f /host/config/trusted_hosts ]] \
        || cat <<EOF > /host/config/trusted_hosts
127.0.0.0/8
172.16.0.0/12
EOF

    # virtual_alias_domains
    echo $VIRTUAL_DOMAINS | tr ' ' "\n" > /host/config/virtual_alias_domains

    # virtual_alias_maps
    [[ -f /host/config/virtual_alias_maps ]] \
        || cat <<EOF > /host/config/virtual_alias_maps
# postmaster@$mydomain    $FORWARD_ADDRESS
EOF
    postmap /host/config/virtual_alias_maps

    # virtual_alias_maps.regexp
    [[ -f /host/config/virtual_alias_maps.regexp ]] \
        || cat <<EOF > /host/config/virtual_alias_maps.regexp
/^postmaster@/    $FORWARD_ADDRESS
/^abuse@/         $FORWARD_ADDRESS
/^root@/          $FORWARD_ADDRESS
/^admin@/         $FORWARD_ADDRESS
/^info@/          $FORWARD_ADDRESS

### Uncomment this to catch all email addresses (not recommended).
#/.*/    $FORWARD_ADDRESS

EOF
    postmap /host/config/virtual_alias_maps.regexp

    # remove sensitive information from email headers
    postconf \
        'smtp_header_checks = regexp:/host/config/smtp_header_checks'
    [[ -f /host/config/smtp_header_checks ]] \
        || cat <<EOF > /host/config/smtp_header_checks
# remove sensitive information from email headers
/^Received:/            IGNORE
EOF
    postmap /host/config/smtp_header_checks

    # activate ports 587 and 465
    sed -i /etc/postfix/master.cf \
        -e 's/^#submission/submission/' \
        -e 's/^#smtps/smtps/'

    # restrictions
    postconf-set \
        'smtpd_sender_restrictions' \
        permit_mynetworks \
        reject_unknown_sender_domain #\
        #reject_unknown_client_hostname
    postconf-set \
        'smtpd_relay_restrictions' \
        reject_unknown_recipient_domain \
        permit_mynetworks \
        permit_sasl_authenticated \
        reject_unauth_destination
    postconf-set \
        'smtpd_recipient_restrictions' \
        permit_mynetworks \
        permit_sasl_authenticated \
        reject_unauth_destination
}

tls_settings() {
    postconf \
        'smtp_tls_security_level = may' \
        'smtpd_tls_security_level = may' \
        'smtp_tls_note_starttls_offer = yes' \
        'smtp_tls_loglevel = 1' \
        'smtpd_tls_loglevel = 1' \
        'smtpd_tls_received_header = yes' \
        'smtpd_tls_cert_file = /host/sslcert/fullchain.pem' \
        'smtpd_tls_key_file = /host/sslcert/privkey.pem'
}

setup_postsrsd() {
    # for more details see:
    # - http://www.open-spf.org/SRS/
    # - https://blog.jak-linux.org/2019/01/05/setting-up-an-email-server-part1/

    local mydomain=$(echo $VIRTUAL_DOMAINS | cut -d' ' -f1)
    sed -i /etc/default/postsrsd \
        -e "/SRS_DOMAIN/ c SRS_DOMAIN=$mydomain"
    systemctl restart postsrsd

    postconf \
        'recipient_canonical_maps = tcp:localhost:10002' \
        'recipient_canonical_classes = envelope_recipient,header_recipient' \
        'sender_canonical_maps = tcp:localhost:10001' \
        'sender_canonical_classes = envelope_sender'
}

setup_spf_policy() {
    # for more details see:
    # https://www.linuxbabe.com/mail-server/setting-up-dkim-and-spf

    sed -i /etc/postfix/master.cf \
        -e '/^policyd-spf/,+1 d'
    cat <<EOF >> /etc/postfix/master.cf
policyd-spf  unix  -  n  n  -  0  spawn
    user=policyd-spf
    argv=/usr/bin/policyd-spf
EOF

    postconf \
        'policyd-spf_time_limit = 3600'
    postconf-append \
        'smtpd_recipient_restrictions' \
        check_policy_service unix:private/policyd-spf

    cat <<EOF > /etc/postfix-policyd-spf-python/policyd-spf.conf
debugLevel = 1
TestOnly = 1
HELO_reject = Fail
Mail_From_reject = Fail
PermError_reject = True
TempError_Defer = True
skip_addresses = 127.0.0.0/8,172.16.0.0/12
EOF
}

setup_opendkim() {
    # config file
    sed -i /etc/opendkim.conf \
        -e '/^Canonicalization/,$ d'
    cat <<EOF >> /etc/opendkim.conf
Canonicalization    relaxed/simple
AutoRestart         yes
AutoRestartRate     10/1M
Background          yes
DNSTimeout          5
SignatureAlgorithm  rsa-sha256

# Map domains in From addresses to keys used to sign messages
KeyTable           /etc/opendkim/key.table
SigningTable       refile:/etc/opendkim/signing.table

# Hosts to ignore when verifying signatures
ExternalIgnoreList  /etc/opendkim/trusted.hosts
InternalHosts       /etc/opendkim/trusted.hosts

UserID opendkim
UMask  007
Socket local:/var/spool/postfix/opendkim/opendkim.sock
PidFile /run/opendkim/opendkim.pid
EOF

    # configure the socket
    mkdir /var/spool/postfix/opendkim
    chown opendkim:opendkim /var/spool/postfix/opendkim
    adduser postfix opendkim

    # Hosts to ignore when verifying signatures
    mkdir /etc/opendkim
    cat <<EOF > /etc/opendkim/trusted.hosts
127.0.0.1/8
172.16.0.0/12
EOF

    # reload the service
    systemctl reload opendkim

    # connect postfix to opendkim
    postconf \
        'milter_default_action = accept' \
        'smtpd_milters = local:/opendkim/opendkim.sock' \
        'non_smtpd_milters = local:/opendkim/opendkim.sock'
}

setup_ldap() {
    postconf-append \
        'virtual_alias_maps' \
        'ldap:/host/config/virtual_alias_maps.ldap'
    postconf-set \
        'smtpd_sender_login_maps' \
        'ldap:/host/config/smtpd_sender_login_maps.ldap'

    [[ -f /host/config/virtual_alias_maps.ldap ]] \
        || cat <<EOF > /host/config/virtual_alias_maps.ldap
version   = 3
start_tls = yes
bind      = yes

server_host = $LDAP_SERVER_HOST
bind_dn     = $LDAP_BIND_DN
bind_pw     = $LDAP_BIND_PW
search_base = $LDAP_SEARCH_BASE

query_filter     = (uid=%s)
result_attribute = mail
EOF

    [[ -f /host/config/smtpd_sender_login_maps.ldap ]] \
        || cat <<EOF > /host/config/smtpd_sender_login_maps.ldap
version   = 3
start_tls = yes
bind      = yes

server_host = $LDAP_SERVER_HOST
bind_dn     = $LDAP_BIND_DN
bind_pw     = $LDAP_BIND_PW
search_base = $LDAP_SEARCH_BASE

query_filter     = (uid=%s)
result_attribute = uid
EOF
}

setup_saslauthd() {
    # /etc/saslauthd.conf
    cat <<EOF > /etc/saslauthd.conf
ldap_servers: ldap://$LDAP_SERVER_HOST

ldap_auth_method: bind
ldap_bind_dn: $LDAP_BIND_DN
ldap_bind_pw: $LDAP_BIND_PW

ldap_search_base: $LDAP_SEARCH_BASE
ldap_scope: sub
ldap_filter: (&(uid=%u)(objectClass=inetOrgPerson))

ldap_start_tls: yes
ldap_tls_check_peer: no

ldap_referrals: yes
log_level: 10
EOF
    chown root:sasl /etc/saslauthd.conf
    chmod 640 /etc/saslauthd.conf
    adduser postfix sasl

    # /etc/postfix/sasl/smtpd.conf
    cat <<EOF > /etc/postfix/sasl/smtpd.conf
pwcheck_method: saslauthd
mech_list: PLAIN LOGIN
log_level: 3
EOF

    # /etc/default/saslauthd
    sed -i /etc/default/saslauthd \
        -e '1 i START=yes' \
        -e '/^MECHANISMS/ c MECHANISMS="ldap"' \
        -e '/^OPTIONS/ c OPTIONS="-r -c -m /var/spool/postfix/var/run/saslauthd"'

    # /var/spool/postfix/var/run/saslauthd
    systemctl stop saslauthd
    mkdir -p /var/spool/postfix/var/run/saslauthd
    rm -rf /var/run/saslauthd
    ln -s /var/spool/postfix/var/run/saslauthd /var/run/saslauthd
    systemctl daemon-reload
    systemctl enable saslauthd
    systemctl start saslauthd

    # try to authenticate the sender before rejecting
    postconf \
        'smtpd_sasl_auth_enable = yes' \
        'broken_sasl_auth_clients = yes'
    postconf-set \
        'smtpd_sender_restrictions' \
        reject_authenticated_sender_login_mismatch \
        permit_mynetworks \
        permit_sasl_authenticated \
        reject_unknown_sender_domain #\
        #reject_unknown_client_hostname
}

prevent_spam_early() {
    # for more details see:
    # https://www.linuxbabe.com/mail-server/block-email-spam-postfix

    # settings to prevent spam early
    postconf \
        'inet_protocols = ipv4'
    postconf-set \
        'smtpd_client_restrictions' \
        permit_mynetworks \
        permit_sasl_authenticated \
        reject_unauth_destination \
        reject_unauth_pipelining
    postconf \
        'smtpd_helo_required = yes'
    postconf-set \
        'smtpd_helo_restrictions' \
        permit_mynetworks \
        permit_sasl_authenticated \
        reject_invalid_helo_hostname \
        reject_non_fqdn_helo_hostname \
        reject_unknown_helo_hostname
    postconf \
        'smtpd_data_restrictions = reject_unauth_pipelining'
    postconf \
        'disable_vrfy_command = yes'
    postconf \
        'tls_ssl_options = NO_COMPRESSION, NO_RENEGOTIATION'
}

setup_postscreen() {
    # for more details see:
    # https://www.linuxbabe.com/mail-server/configure-postscreen-in-postfix-to-block-spambots

    # make sure that the needed config lines on master.cf are not commented
    sed -i /etc/postfix/master.cf \
        -e '/^#smtp .* smtpd$/ s/^#smtp /smtp /' \
        -e '/^#smtp .* postscreen$/ s/^#smtp /smtp /' \
        -e '/^#smtpd .* smtpd$/ s/^#smtpd /smtpd /' \
        -e '/^#dnsblog/ s/^#dnsblog/dnsblog/' \
        -e '/^#tlsproxy/ s/^#tlsproxy/tlsproxy/'

    # create permanent whitelist/blacklist
    postconf-set \
        'postscreen_access_list' \
        permit_mynetworks \
        cidr:/host/config/postscreen_access.cidr
    postconf \
        "postscreen_blacklist_action = drop"
    [[ -f /host/config/postscreen_access.cidr ]] \
        || cat <<EOF > /host/config/postscreen_access.cidr
### permit the public ip of the server or the vpn ip (if any)
#12.34.56.78/32    permit
#10.10.10.1/32     permit

### permanently blacklist an ip address
#23.45.67.89/32     reject
EOF

    # drop an smtp client if it fails the pregreet test
    postconf "postscreen_greet_action = drop"

    # use some public blacklists & whitelists
    postconf \
        "postscreen_dnsbl_threshold = 3" \
        "postscreen_dnsbl_action = enforce"
    postconf-set \
        postscreen_dnsbl_sites \
        'zen.spamhaus.org*3' \
        'b.barracudacentral.org=127.0.0.[2..11]*2' \
        'bl.spameatingmonkey.net*2' \
        'bl.spamcop.net' \
        'dnsbl.sorbs.net' \
        'swl.spamhaus.org*-4' \
        'list.dnswl.org=127.[0..255].[0..255].0*-2' \
        'list.dnswl.org=127.[0..255].[0..255].1*-4' \
        'list.dnswl.org=127.[0..255].[0..255].[2..3]*-6'

    # enable deep protocol tests
    postconf \
        "postscreen_pipelining_enable = yes" \
        "postscreen_pipelining_action = enforce" \
        "postscreen_non_smtp_command_enable = yes" \
        "postscreen_non_smtp_command_action = enforce" \
        "postscreen_bare_newline_enable = yes" \
        "postscreen_bare_newline_action = enforce" \
        "postscreen_dnsbl_whitelist_threshold = -2"

    #systemctl reload postfix
}

setup_postwhite() {
    # for more details see:
    # https://www.linuxbabe.com/mail-server/configure-postscreen-in-postfix-to-block-spambots

    # install spf-tools and postwhite
    cd /usr/local/bin/
    git clone https://github.com/spf-tools/spf-tools.git
    git clone https://github.com/stevejenkins/postwhite.git
    cd -
    cp /usr/local/bin/postwhite/postwhite.conf /etc/

    # add the whitelist file to the postfix config
    postconf-append \
        'postscreen_access_list' \
        cidr:/etc/postfix/postscreen_spf_whitelist.cidr
    touch /host/config/postscreen_spf_whitelist.cidr
    ln -s /host/config/postscreen_spf_whitelist.cidr /etc/postfix/

    # create a cron job to run postwhite daily
    cat <<EOF > /etc/cron.daily/postwhite
#!/bin/bash
### update postscreen whitelists
/usr/local/bin/postwhite/postwhite &>/dev/null
postfix reload
EOF
    chmod +x /etc/cron.daily/postwhite

    # create a cron job to run scrape_yahoo weekly
    cat <<EOF > /etc/cron.weekly/scrape_yahoo
#!/bin/bash
### update Yahoo! IPs for postscreen whitelists
/usr/local/bin/postwhite/scrape_yahoo > /dev/null 2>&1
EOF
    chmod +x /etc/cron.weekly/scrape_yahoo

    # run postwhite
    #/usr/local/bin/postwhite/postwhite

    #systemctl reload postfix
}

setup_pflogsumm() {
    # for more details see:
    # https://www.linuxbabe.com/mail-server/configure-postscreen-in-postfix-to-block-spambots

    # run pflogsumm weekly with a cron job
    local mydomain=$(echo $VIRTUAL_DOMAINS | cut -d' ' -f1)
    cat <<EOF > /etc/cron.d/pflogsumm
MAILTO=postmaster@$mydomain
0 4 * * 0 root /usr/sbin/pflogsumm /var/log/mail.log --problems-first --rej-add-from --verbose-msg-detail -q
EOF
}

# call the main function
main "$@"
