# Postfix container

## About

This is a simple and minimal smtp server based on postfix. Instead of
being a full-fledged email server, it supports only these use cases:

1. Sending email only from trusted hosts
2. Forward-only email accounts with LDAP authentication

### 1. Sending email only from trusted hosts

Trusted hosts are the local host, docker containers on the host, and
specific IP addresses/networks that are listed on a configuratin file
(`config/trusted_hosts`). Allowing only the trusted hosts to send
emails prevents any spammers from abusing the mail server.

This setup is the default one and can be used for sending email
notifications from web applications like: Gitea, NextCloud, Moodle,
etc.

It supports multiple email domains and multiple email addresses for
each domain (for example: `info@example.org`, `admin@example.org`,
`noreply@example.org`, etc.). By default, all these are NOREPLY email
addresses, which means that emails that are sent to them are rejected.

However we can forward some of these email addresses to an external
email address, if we want. It is also possible to catch all the
received emails and send them to an external email address (like
`user@gmail.com`).

### 2. Forward-only email accounts with LDAP authentication

This use case extends the previous one with LDAP authentication.  This
means that user accounts and user passwords are stored on a LDAP
server. The mail server will allow you to send email only if you
authenticate with a valid username and password (which is checked on
the LDAP server).

If someone sends email to one of the mail domains supported by the
server, the recipient address will be checked on the LDAP directory
whether it is a valid user, before the email is accepted. The received
emails are not stored locally, but are forwarded instead to an
external email address that belongs to the user (and is recorded in
the LDAP directory).

This setup could be useful for organizations or companies. It allows
them to have branded email addresses and to use their own mail server
for sending emails.

However it spares their users from having multiple email accounts,
allowing them to receive all the emails in a single mailbox, on their
prefered mail provider (for example `gmail.com`). At the same time, it
relieves postmasters of the organization from having to manage
mailboxes and backups, scanning emails for viruses and spam, etc.
These are not easy tasks and they can be done much more professionally
from the email providers.

For this setup we obviously need to have an LDAP server that contains
the user data (username, password and forward email). We also need to
use the LDAP settings to enable it.

## Setup

### DNS configuration

To send and receive emails, DNS has to be configured properly. For
each email domain you need something like this on the DNS
configuration:

```
smtp.example.org.    IN    A           10.11.12.13
example.org.         IN    MX    10    smtp.example.org.
example.org.         IN    MX    20    smtp.example.org.
example.org.         IN    TXT         "v=spf1 mx -all"
```


### Installation

  - First install `ds` and `revproxy`:
      + https://gitlab.com/docker-scripts/ds#installation
      + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull postfix`

  - Create a directory for the container: `ds init postfix @smtp.example.org`

  - Fix the settings: `cd /var/ds/smtp.example.org/ ; vim settings.sh`

  - Make the container: `ds make`

  - Quick test: `ds test1`


### Configuration

Postfix configuration files are on the directory `config/`. There you
can add more email domains, aliases, or allow hosts/networks to use
the server for relaying (sending) emails. You need to run `ds inject
update.sh` after making changes to the config files.

If you add a new email domain, you should also create a new DKIM key
for it and add the corresponding configuration as a TXT record on the
DNS server: `ds dkimkey add <email-domain>`

## Docs

1. [Send email only from trusted hosts](/docs/1-send-email-only-from-trusted-hosts.md)
2. [Forward-only email accounts with LDAP authentication](/docs/2-forward-only-accounts-with-ldap-auth.md)


### Further readings and info

- https://www.binarytides.com/postfix-mail-forwarding-debian/
- https://www.linuxbabe.com/mail-server/setting-up-dkim-and-spf
- https://blog.edmdesigner.com/send-email-from-linux-command-line/
