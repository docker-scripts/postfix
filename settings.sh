APP=postfix

PORTS='25:25 587:587 465:465'

HOSTNAME='smtp.example.org'
VIRTUAL_DOMAINS='
    example.org
'

# All the received mail will be forwarded to this address.
# This can be customized on 'config/virtual_alias_maps'
# and 'config/virtual_alias_maps.regexp'
FORWARD_ADDRESS='user@mail.com'

### Uncomment to enable LDAP checking and authentication
#LDAP_SERVER_HOST='ldap.example.org'
#LDAP_SEARCH_BASE='ou=users,dc=example,dc=org'
#LDAP_BIND_DN='cn=mailserver,dc=example,dc=org'
#LDAP_BIND_PW='pass123'
