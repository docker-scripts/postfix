This page is deprecated. It has been reorganized in the following doc
pages:

- [Simple SMTP with GMail](/docs/0-ssmtp-with-gmail.md)
- [Send email only from trusted hosts](/docs/1-send-email-only-from-trusted-hosts.md)
- [Forward-only email accounts with LDAP authentication](/docs/2-forward-only-accounts-with-ldap-auth.md)

