#!/bin/bash

rename_function cmd_remove orig_cmd_remove
cmd_remove() {
    rm -f /etc/cron.d/"$(echo $(basename $(pwd)) | tr . -)"-copy-ssl-cert
    orig_cmd_remove
}

dexec() {
    docker exec -u root $CONTAINER env TERM=xterm "$@"
}

runtest() {
    local description=$1 ; shift
    local subject=$(echo "$description" | head -1)

    echo "$description"
    read
    "$@" --h-Subject "$subject" | less
    dexec tail /var/log/mail.log | less
}
