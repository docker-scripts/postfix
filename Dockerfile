include(bookworm)

RUN <<EOF
  # install postfix
  DEBIAN_FRONTEND=noninteractive \
  apt install --yes \
      dnsutils \
      postfix \
      postfix-ldap \
      postfix-policyd-spf-python \
      opendkim \
      opendkim-tools \
      sasl2-bin \
      pflogsumm \
      postsrsd \
      swaks

  apt install --yes \
      git \
      curl \
      fail2ban \
      iptables
EOF