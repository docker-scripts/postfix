cmd_config_help() {
    cat <<_EOF
    config
        Customized configuration of the container.

_EOF
}

rename_function cmd_config standard_config
cmd_config() {
    standard_config
    ds wks add wks.example.org 10025 example.org
}
