#!/bin/bash -x
### Customize postfix configuration.
### This script is called automatically by 'ds inject update.sh'
### or it may be called manually by 'ds exec config/custom.sh'

### Example:
# postconf 'transport_maps = hash:/host/config/transport_maps'
# postmap /host/config/transport_maps
# postfix reload
