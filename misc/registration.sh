cmd_registration_help() {
    cat <<_EOF
    registration [enable | disable]
        Allow users to get automatically a forward email account
        by sending a mail to register@example.org
        This might be useful for testing.

_EOF
}

cmd_registration() {
    local cmd=$1
    case $cmd in
        enable) _enable ;;
        disable) _disable ;;
        *) fail "Usage:\n$(cmd_registration_help)\n" ;;
    esac
}

_enable() {
    # install some packages
    apt install --yes swaks pwgen
    ds exec apt install --yes maildrop

    # create an account for user 'register'
    mkdir -p register
    ds exec useradd -m -d /host/register register
    ds exec chown register: /host/register

    # forward registration emails to the local account
    local mydomain=$(echo $VIRTUAL_DOMAINS | cut -d' ' -f1)
    sed -i config/virtual_alias_maps -e '/register@localhost/d'
    echo "register@$mydomain  register@localhost" >> config/virtual_alias_maps
    ds exec postmap config/virtual_alias_maps

    # store emails to the directory Mail on the home
    ds exec postconf 'mailbox_command = /usr/bin/maildrop -d ${USER}'
    ds exec postfix reload
    cat <<EOF > register/.mailfilter
logfile maildrop.log
to Mail
EOF
ds exec chmod 600 /host/register/.mailfilter
ds exec touch /host/register/maildrop.log
ds exec rm -rf /host/register/Mail
ds exec maildirmake /host/register/Mail
ds exec chown register: -R /host/register/

    # install a cron job
    local appdir=$(basename $(pwd))
    cat <<EOF > /etc/cron.d/autoregister
*/3 * * * * root  bash -l -c "ds @$appdir register-cron"
EOF
}

_disable() {
    rm -f /etc/cron.d/autoregister
    sed -i config/virtual_alias_maps -e '/register@localhost/d'
    ds exec postmap config/virtual_alias_maps

    ds exec userdel register
    rm -rf register
    ds exec postconf 'mailbox_command = '
    ds exec postfix reload
    ds exec apt purge --yes maildrop
}
