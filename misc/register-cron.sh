cmd_register-cron_help() {
    cat <<EOF
    register-cron
        Called from a cron job in order to complete registration requests,
        to cleanup expired registrations, etc
EOF
}

cmd_register-cron() {
    _handle_new_requests
    _cleanup_expired_users
}

_handle_new_requests() {
    local messages msg email user pass mydomain user_email

    messages=$(ls register/Mail/new/* 2>/dev/null)
    [[ -z $messages ]] && return
    
    for msg in $messages; do
        email=$(cat $msg | grep '^Return-Path:' | cut -d' ' -f2)
        rm -f $msg
        [[ -z $email ]] && continue

        # generate a new random user and pass
        user=$(pwgen)
        pass=$(pwgen)

        # save to a file for keeping track
        mkdir -p register/users
        cat <<EOF > register/users/$user
$user $email $pass
EOF

        # create a user account on LDAP
        mydomain=$(echo $VIRTUAL_DOMAINS | cut -d' ' -f1)
        user_email=$user@$mydomain
        ds @ldap.$mydomain \
           user add users $user_email $email $pass

        # send notification email
        cat <<EOF \
            | swaks -tlso -d- \
                    --from register@$mydomain \
                    --to $email \
                    --server $HOSTNAME
From: register@$mydomain
To: $email
Subject: Test email account created

This test email account has been created:
  email address : $user_email
    smtp server : $HOSTNAME
       username : $user_email
       password : $pass
It will be removed automatically after a week.

Emails that are received on this account
will be forwarded to: $email

EOF
    done
}

_cleanup_expired_users() {
    local users user uid email mydomain user_email
    users=$(find register/users -mtime +7)
    [[ -z $users ]] & return
    for user in $users; do
        uid=$(cat $user | cut -d' ' -f1)
        email=$(cat $user | cut -d' ' -f2)

        # delete user account from LDAP
        mydomain=$(echo $VIRTUAL_DOMAINS | cut -d' ' -f1)
        user_email=$uid@$mydomain
        ds @ldap.$mydomain \
           user del users $user_email

        # send notification email
        cat <<EOF \
            | swaks -tlso -d- \
                    --from register@$mydomain \
                    --to $email \
                    --server $HOSTNAME
From: register@$mydomain
To: $email
Subject: Test email account expired

This test email account has expired: $user_email


EOF
        rm -f $user
    done
}
